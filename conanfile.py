import os

from conans import ConanFile, CMake

project_name = "fs"


class FsConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/fs"
    description = "Basic filesystem utilities"
    topics = ("asd", project_name)
    generators = "cmake"
    exports_sources = "include*", "src*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.meta/0.0.1@asd/testing"
    )

    def source(self):
        pass

    def build(self):
        pass

    def package(self):
        self.copy("*.h", dst="include", src="include")

    def package_info(self):
        self.info.header_only()
