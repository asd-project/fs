//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <vector>
#include <string>

#include <filesystem>
#include <fstream>

#include <meta/macro.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace fs
    {
        using namespace std::filesystem;

        std::optional<fs::path> find_file(const fs::path & dir_path, const auto & file_name) {
            fs::recursive_directory_iterator begin(dir_path);
            fs::recursive_directory_iterator end;

            const auto it = std::find_if(begin, end, [&file_name](const fs::directory_entry & e) {
                return e.path().filename().compare(file_name) == 0;
            });

            if (it == end) {
                return std::nullopt;
            }

            return it->path();
        }

        template <class Handler>
        auto read(const fs::path & file_path, Handler && handler) {
            if (!fs::exists(file_path)) {
                BOOST_THROW_EXCEPTION(std::runtime_error("Path " + file_path.string() + " doesn't exist"));
            }

            std::ifstream stream(file_path.string());
            return handler(stream, file_path);
        }

        template <class Alloc = std::allocator<std::byte>>
        auto read_bytes(const fs::path & file_path, Alloc alloc = {}) {
            return fs::read(file_path, [&](auto && stream, auto && path) {
                std::vector<std::byte, Alloc> data(fs::file_size(path), alloc);
                stream.read(reinterpret_cast<char *>(data.data()), data.size());

                BOOST_ASSERT_MSG(stream, "Couldn't read all characters");

                return data;
            });
        }

        template <class Alloc = std::allocator<std::byte>>
        auto read_bytes(const fs::path & file_path, size_t length, Alloc alloc = {}) {
            return fs::read(file_path, [&](auto && stream, auto &&) {
                std::vector<std::byte, Alloc> data(length, alloc);
                stream.read(reinterpret_cast<char *>(data.data()), data.size());

                BOOST_ASSERT_MSG(stream, "Couldn't read all characters");

                return data;
            });
        }

        template <class Alloc = std::allocator<char>>
        auto read_string(const fs::path & file_path, Alloc alloc = {}) {
            return fs::read(file_path, [&](auto && stream, auto && path) {
                std::basic_string<char, std::char_traits<char>, Alloc> s(fs::file_size(path), '\0', alloc);
                stream.read(&s[0], s.size());

                return s;
            });
        }

        template <class Alloc = std::allocator<char>>
        auto read_string(const fs::path & file_path, size_t length, Alloc alloc = {}) {
            return fs::read(file_path, [&](auto && stream, auto &&) {
                std::basic_string<char, std::char_traits<char>, Alloc> s(length, '\0', alloc);
                stream.read(&s[0], s.size());

                return s;
            });
        }
    }
}

template <class S, class R = decltype(std::declval<asd::fs::path &>().concat(std::declval<S>()))>
R operator + (const asd::fs::path & path, S && s) {
    return asd::fs::path(path).concat(std::forward<S>(s));
}

template <class S, class R = decltype(std::declval<asd::fs::path &>().concat(std::declval<S>()))>
R operator + (asd::fs::path && path, S && s) {
    return path.concat(std::forward<S>(s));
}
